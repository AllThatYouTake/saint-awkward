module.exports = {
  root: true,
  parserOptions: {
    ecmaVersion: 6,
    sourceType: 'module',
  },
  extends: [
    'eslint:recommended',
    'plugin:ember-suave/recommended',
  ],
  env: {
    'browser': true,
    'es6': true,
  },
  rules: {
    'keyword-spacing': 'off',
    'comma-dangle': ['error', 'always-multiline'],
  },
};
