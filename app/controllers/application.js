import Ember from 'ember';
import injectService from 'ember-service/inject';

const {
  Controller,
  get,
  getProperties,
  set,
  setProperties,
} = Ember;

export default Controller.extend({
  session: injectService(),

  errorMessage: null,
  showLoginPane: false,

  actions: {
    toggleLoginPane() {
      this.toggleProperty('showLoginPane');
    },

    login() {
      let { email, password, session } = getProperties(this, 'email', 'password', 'session');
      session.open('firebase', { provider: 'password', email, password })
        .then(() => {
          this.toggleProperty('showLoginPane');
          setProperties(this, {
            errorMessage: null,
            email: null,
            password: null,
          });
        })
        .catch((reason) => set(this, 'errorMessage', reason.error || reason));
    },

    logout() {
      get(this, 'session').close();
    },
  },
});
