import Ember from 'ember';

const {
  Service,
} = Ember;

export default Service.extend({
  isLoggedIn: false,
  isPatreon: false,

  login() {},
  load() {},
});
