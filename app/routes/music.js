import Ember from 'ember';
import injectService from 'ember-service/inject';

const {
  get,
  Route,
} = Ember;

export default Route.extend({
  patreon: injectService(),

  beforeModel() {
    get(this, 'patreon').load();
  },
});
